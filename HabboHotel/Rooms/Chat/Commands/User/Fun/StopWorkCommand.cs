﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plus.HabboHotel.Users;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class StopWorkCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_stopwork"; }
        }
        public string Parameters
        {
            get { return ""; }
        }
        public string Description
        {
            get { return "Stop working"; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            Jobs Job = new Jobs();

            Job.StopWorking(Session);
        }
    }
}
