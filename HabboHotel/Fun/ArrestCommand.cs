﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class ArrestCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_arrest"; }
        }
        public string Parameters
        {
            get { return "%username% %time%"; }
        }
        public string Description
        {
            get { return "Arrest a player"; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.JobId == 1)
            {
                GameClient Target = PlusEnvironment.GetGame().GetClientManager().GetClientByUsername(Params[1]);
                if (Target == null)
                    return;

                int Time = 0;

                if (int.TryParse(Params[2], out Time))
                {
                    Target.GetHabbo().Roleplay.ArrestUser(Time, false);
                    Session.GetHabbo().Roleplay.Chat("*Arrests " + Target.GetHabbo().Username + " for " + Time + " minutes*");
                }
            }
        }
    }
}