﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class CreateGangCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_create_gang"; }
        }
        public string Parameters
        {
            get { return "%name%"; }
        }
        public string Description
        {
            get { return "Create a gang"; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("You must enter a gang name!");
                return;
            }

            string GangName = Params[1];
            Session.GetHabbo().Roleplay.CreateGang(Session, GangName);
            Session.GetHabbo().Roleplay.HasGang = true;
            Session.GetHabbo().Roleplay.MoreGangChanges(Session);
        }
    }
}