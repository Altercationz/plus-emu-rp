﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class TaxiCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_taxi"; }
        }
        public string Parameters
        {
            get { return "%roomid%"; }
        }
        public string Description
        {
            get { return "Call a taxi to another room"; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().Roleplay.GoingToId > 0)
            {
                Session.GetHabbo().Roleplay.Chat("*Cancels their taxi*");
                return;
            }

            if (Session.GetHabbo().Roleplay.InJail)
                return;

            int Id = Convert.ToInt32(Params[1]);

            if (Id == Session.GetHabbo().CurrentRoomId)
                return;

            if (Session.GetHabbo().Credits < 15)
            {
                Session.SendWhisper("You cannot afford the taxi! ($15)");
                return;
            }

            Session.GetHabbo().Roleplay.CallTaxi(Id);
        }
    }
}