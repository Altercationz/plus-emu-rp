﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class DeleteGangCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_delete_gang"; }
        }
        public string Parameters
        {
            get { return ""; }
        }
        public string Description
        {
            get { return "Delete your current gang"; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            Session.GetHabbo().Roleplay.DeleteGang(Session, Session.GetHabbo().Roleplay.GangName);
        }
    }
}