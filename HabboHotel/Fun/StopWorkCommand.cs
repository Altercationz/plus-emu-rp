﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Plus.Utilities;
using Plus.Core;
using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Quests;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.HabboHotel.Users;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class StopWorkCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_stopwork"; }
        }
        public string Parameters
        {
            get { return ""; }
        }
        public string Description
        {
            get { return "Stop working"; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            Jobs Job = new Jobs();

            if (Session.GetHabbo().Roleplay.IsWorking == false)
            {
                Session.SendWhisper("You are not working!");
                return;
            }

            if (Session.GetHabbo().Roleplay.JobId == 0 && Session.GetHabbo().Roleplay.JobRank == 0)
            {
                Session.GetHabbo().Roleplay.Chat("*Stops collecting welfare*");
                Job.StopWorkTimer(Session);
                Session.GetHabbo().Roleplay.IsWorking = false;

                RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                if (User == null)
                    return;
                   
                Session.SendMessage(new UserChangeComposer(User, true));
                Session.GetHabbo().CurrentRoom.SendMessage(new UserChangeComposer(User, false));
            }
            else
            {
                RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                if (User == null)
                    return;

                Session.GetHabbo().Roleplay.Chat("*Stops working*");
                Session.GetHabbo().Motto = "Civilian";
                Job.StopWorking(Session);
            }
        }
    }
}