﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Timers;

using log4net;

using Plus.Core;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.Users.Badges;
using Plus.HabboHotel.Users.Inventory;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;
using Plus.HabboHotel.Users.UserDataManagement;

using Plus.HabboHotel.Users.Process;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;


using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.HabboHotel.Users.Effects;
using Plus.HabboHotel.Users.Messenger.FriendBar;
using Plus.HabboHotel.Users.Clothing;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Chat.Commands;
using Plus.HabboHotel.Users.Permissions;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users.Calendar;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Users
{
    public class Jobs
    {
        private static readonly ILog Log = LogManager.GetLogger("Plus.HabboHotel.Users.Jobs");

        public Jobs()
        {
        }

        // Timers & Tasks
        public Timer WorkTimer;
        public Task WorkTask;
        GameClient Session;


        public void StartWorkTimer(GameClient Session, int Amount)
        {
            WorkTimer = new Timer();
            WorkTimer.Interval = 60000;
            WorkTimer.AutoReset = false;
            WorkTimer.Elapsed += WorkTimer_Elapsed;

            Task WorkTask = new Task(WorkTimer.Start);
            WorkTask.Start();
        }

        public void PayUser(GameClient Session, int Amount)
        {
            if (!WorkTimer.Enabled || WorkTimer == null)
            {
                if (!Session.GetHabbo().Roleplay.IsWorking)
                {
                    Amount = Session.GetHabbo().Roleplay.Payment;
                    Session.GetHabbo().Credits += Amount;
                    Session.GetHabbo().GetClient().SendMessage(new CreditBalanceComposer(Session.GetHabbo().Credits));

                    Session.SendNotification("You have been paid " + Amount + "!");
                    StartWorkTimer(Session, Session.GetHabbo().Roleplay.Payment);

                    Session.GetHabbo().Roleplay.OnTheClock = DateTime.Now;
                }
            }
        }

        public void WorkTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PayUser(Session, 3);
        }

        public void StopWorkTimer(GameClient Session)
        {
            if (WorkTimer != null || WorkTimer.Enabled)
            {
                WorkTimer.Stop();
            }
        }


        public void UpdateClothing(GameClient Session, string Look)
        {
            // seperate face from clothing
            string[] Split = Session.GetHabbo().Look.Split('.');
            string Face = null;
            string Hair = null;

            foreach (string Clothing in Split)
            {
                if (Clothing.StartsWith("hd"))
                {
                    Face = Clothing;
                }
                else if (Clothing.StartsWith("hr"))
                {
                    Hair = Clothing;
                }
            }
            RoomUser User = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Username);
            Look += "." + Face + "." + Hair;

            Session.GetHabbo().Look = Look;

            Session.SendMessage(new UserChangeComposer(User, true));
            Session.GetHabbo().CurrentRoom.SendMessage(new UserChangeComposer(User, false));
        }


        public void StartWork(GameClient Session, int JobId, int JobRank)
        {
            string RankName = null;
            int Pay = 0;
            string MaleUni = null;
            string FemaleUni = null;
            string JobMotto = null;
            string JobName = null;
            int JobRoomId = 0;
            bool CanExitHeadquarters = false;

            if (Session == null)
                return;

            Room Room = Session.GetHabbo().CurrentRoom;
            if (Room == null)
                return;

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            if (Session.GetHabbo().Roleplay.IsWorking)
            {
                Session.SendWhisper("You are already working!");
                return;
            }

            if (JobId == 0 && JobRank == 0)
            {
                Session.GetHabbo().Roleplay.IsWorking = true;
                Session.GetHabbo().Roleplay.Payment = 15;
                Session.GetHabbo().Roleplay.CanExitHeadQuarters = true;
                Session.SendMessage(new UserChangeComposer(User, true));
                Session.GetHabbo().CurrentRoom.SendMessage(new UserChangeComposer(User, false));

                Session.GetHabbo().Roleplay.Chat("*Starts collecting welfare*");
                Session.GetHabbo().Roleplay.StartWorkTimer(15);
            }
            else
            {
                using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    Adapter.SetQuery("SELECT a.*,b.* FROM rp_jobs AS A JOIN rp_ranks AS b ON (a.id = b.job_id) WHERE b.job_id = @jobid AND b.rank_id = @rankid");
                    Adapter.AddParameter("jobid", JobId);
                    Adapter.AddParameter("rankid", JobRank);
                    Adapter.RunQuery();

                    DataTable Table = Adapter.getTable();

                    if (Table != null)
                    {
                        foreach (DataRow Row in Table.Rows)
                        {
                            JobId = Convert.ToInt32(Row["job_id"]);
                            RankName = Convert.ToString(Row["job_name"]);
                            Pay = Convert.ToInt32(Row["pay"]);
                            MaleUni = Convert.ToString(Row["male_uni"]);
                            FemaleUni = Convert.ToString(Row["female_uni"]);
                            JobMotto = Convert.ToString(Row["motto"]);
                            JobRoomId = Convert.ToInt32(Row["roomid"]);
                            JobName = Convert.ToString(Row["rank_name"]);
                            CanExitHeadquarters = PlusEnvironment.EnumToBool(Row["exithq"].ToString());

                            if (Session.GetHabbo().CurrentRoomId != JobRoomId)
                            {
                                Session.SendWhisper("You must be in your job headquarters to start working!");
                                return;
                            }

                            Session.GetHabbo().Roleplay.LookBeforeWork = Session.GetHabbo().Look;
                            this.UpdateClothing(Session, (Session.GetHabbo().Gender.ToLower() == "m" ? MaleUni : FemaleUni));

                            
                            Session.GetHabbo().Motto = "[Working] " + JobName;
                            Session.GetHabbo().Roleplay.Payment = Pay;
                            Session.GetHabbo().Roleplay.IsWorking = true;
                            Session.GetHabbo().Roleplay.CanExitHeadQuarters = CanExitHeadquarters ? true : false;

                            Session.GetHabbo().Roleplay.Chat("*Starts working as a " + JobName + "*");
                            Session.SendWhisper("Your current pay is $" + Pay + " and you will receive it in 10 minutes.");

                            Session.GetHabbo().Roleplay.StartWorkTimer(Session.GetHabbo().Roleplay.Payment);

                            Session.SendMessage(new UserChangeComposer(User, true));
                            Session.GetHabbo().CurrentRoom.SendMessage(new UserChangeComposer(User, false));
                        }
                    }
                }
            }
        }

        public void StopWorking(GameClient Session)
        {
            if (Session == null)
                return;

            Room Room = Session.GetHabbo().CurrentRoom;
            if (Room == null)
                return;

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            Session.GetHabbo().Look = Session.GetHabbo().Roleplay.LookBeforeWork;
            Session.GetHabbo().Roleplay.IsWorking = false;
            Session.GetHabbo().Roleplay.Payment = 0;

            Session.SendMessage(new UserChangeComposer(User, true));
            Session.GetHabbo().CurrentRoom.SendMessage(new UserChangeComposer(User, false));

            StopWorkTimer(Session);
        }
    }
}