﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Timers;
using System.Threading;

using log4net;

using Plus.Core;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.Users.Badges;
using Plus.HabboHotel.Users.Inventory;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;
using Plus.HabboHotel.Users.UserDataManagement;

using Plus.HabboHotel.Users.Process;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;


using Plus.HabboHotel.Users.Navigator.SavedSearches;
using Plus.HabboHotel.Users.Effects;
using Plus.HabboHotel.Users.Messenger.FriendBar;
using Plus.HabboHotel.Users.Clothing;
using Plus.Communication.Packets.Outgoing.Navigator;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Chat.Commands;
using Plus.HabboHotel.Users.Permissions;
using Plus.HabboHotel.Subscriptions;
using Plus.HabboHotel.Users.Calendar;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Users
{
    public class Roleplay
    {
        private static readonly ILog Log = LogManager.GetLogger("Plus.HabboHotel.Users.Roleplay");

        // User Information
        public int UserId;
        public string Motto;
        GameClient user;
        public int FishAmount; public bool IsAfk;


        // Health
        public int Health;

        // Job System
        public bool IsWorking;
        public int JobId;
        public int JobRank;
        public string LookBeforeWork;
        public int Payment;
        public DateTime OnTheClock;
        public bool CanExitHeadQuarters;
        public System.Timers.Timer WorkTimer;
        public Task WorkTask;
        public string OldMotto;
        public bool Afk;

        // Prison System
        public System.Timers.Timer JailTimer;
        public Task JailTask;
        public bool LoggedOffInJail;

        // Saving rp data to db every minute
        public System.Timers.Timer SavingTimer;
        public Task SavingTask;

        // Gang System
        public bool InGang;
        public int GangRank;
        public int GangId;
        public int MyGangId;
        public bool HasPendingInvite;
        public int InvitedToId;
        public string InvitedBy;
        public string MyGangName;
        public string InvitedToName;

        // Jail System
        public string JailMaleUni;
        public string JailFemaleUni;
        public DateTime SentencedAt;
        public bool IsJailed;
        public int Bail;

        // Gang System
        public bool HasGang;
        public string GangName;

        // Fishing System
        public System.Timers.Timer FishingTimer;
        public Task FishingTask;
        public DateTime StartedFishing;

        // Taxi System
        public int GoingToId;

        // Police System
        public bool Wanted;
        public bool InJail;
        public int JailTime;
        public int JailAmount;

        // Load The Data.
        private void LoadUserData()
        {
            if (this.UserId > 0)
            {
                int health = 100;
                bool injail;
                int jailtime = 0;
                int fishamount = 0;
                int jobRank = 0;
                int jobId = 0;
                bool hasgang = false;
                string Gangname = string.Empty;
                int gangId = 0;
                int myGangId = 0;

                using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    Adapter.SetQuery("SELECT * FROM rp_data WHERE userid = @id");
                    Adapter.AddParameter("id", this.UserId);

                    DataRow Row = Adapter.getRow();
                    if (Row != null)
                    {
                        health = Convert.ToInt32(Row["health"]);
                        injail = PlusEnvironment.EnumToBool(Row["in_jail"].ToString());
                        jailtime = Convert.ToInt32(Row["jail_time"]);
                        fishamount = Convert.ToInt32(Row["fish_amount"]);
                        jobRank = Convert.ToInt32(Row["job_rank"]);
                        jobId = Convert.ToInt32(Row["job_id"]);

                        if (jailtime > 0)
                        {
                            ArrestUser(jailtime, true);
                        }
                        else
                        {
                            this.JailAmount = 0;
                            this.SentencedAt = DateTime.Now;
                        }
                    }
                    else
                    {
                        // New User
                        Adapter.SetQuery("INSERT INTO rp_data (userid) VALUES (@id)");
                        Adapter.AddParameter("id", this.UserId);
                        Adapter.RunQuery();
                    }
                    

                    this.Health = health;
                    this.InJail = false;
                    this.JailTime = 0;
                    this.Motto = "Citizen";
                    this.FishAmount = fishamount;
                    this.JobId = jobId;
                    this.JobRank = jobRank;
                    this.HasGang = hasgang;
                    this.GangName = Gangname;
                    this.GangId = gangId;
                    this.MyGangId = myGangId;
                    this.LoggedOffInJail = false;

                    this.StartSaving();
                }
            }
        }

        public Roleplay(int Id)
        {
            this.UserId = Id;
            this.LoadUserData();
        }

        public void SavePlayer(Habbo Player, bool autosaving = false)
        {
            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                int JailMins = 0;
                if (this.InJail)
                {
                    TimeSpan TimeLeft = DateTime.Now - this.SentencedAt;
                    JailMins = TimeLeft.Minutes;
                }

                Adapter.SetQuery("UPDATE users SET home_room = @lastroomid WHERE id = @userid");
                Adapter.AddParameter("lastroomid", Player.CurrentRoomId);
                Adapter.AddParameter("userid", Player.Id);
                Adapter.RunQuery();

                Adapter.SetQuery("UPDATE rp_data SET health = @health, jail_time = @jailtime, fish_amount = @fishamount, job_id = @jobid, job_rank = @jobrank WHERE userid = @id");
                Adapter.AddParameter("health", this.Health);
                Adapter.AddParameter("jailtime", JailMins);
                Adapter.AddParameter("fishamount", this.FishAmount);
                Adapter.AddParameter("jobid", this.JobId);
                Adapter.AddParameter("jobrank", this.JobRank);
                Adapter.AddParameter("id", this.UserId);
                Adapter.RunQuery();

                Log.Info("User Id: " + this.UserId + " has been saved.");
            }

            if (!autosaving)
            {
                this.Dispose();
                Log.Info("User " + this.GetClient.GetHabbo().Username + " saved and disposed.");
            }
        }


        public void StartSaving()
        {
            this.SavingTimer = new System.Timers.Timer();
            this.SavingTimer.AutoReset = false;
            this.SavingTimer.Interval = 60000;
            this.SavingTimer.Elapsed += SavingTimer_Elapsed;

            this.SavingTask = new Task(this.SavingTimer.Start);
            this.SavingTask.Start();
        }

        public void SavingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.GetClient != null && this.GetClient.GetHabbo() != null)
            {
                this.SavePlayer(this.GetClient.GetHabbo(), true);
                this.StartSaving();
            }
            else
                this.GetClient.Disconnect();
        }

        public void Dispose()
        {
            if (this.WorkTimer != null)
                WorkTimer.Dispose();

            if (this.FishingTimer != null)
                FishingTimer.Dispose();

            if (JailTimer != null)
                JailTimer.Dispose();

            if (SavingTimer != null)
                SavingTimer.Dispose();

            if (WorkTask != null)
                WorkTask.Dispose();

            if (FishingTask != null)
                FishingTask.Dispose();

            if (JailTask != null)
                JailTask.Dispose();

            if (SavingTask != null)
                SavingTask.Dispose();
        }


        public void Chat(string msg)
        {
            GameClient User = PlusEnvironment.GetGame().GetClientManager().GetClientByUserID(this.UserId);
            User.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(User.GetHabbo().Username).OnChat(0, msg, false);
        }

        public GameClient GetClient
        {
            get { return PlusEnvironment.GetGame().GetClientManager().GetClientByUserID(this.UserId); }
        }

        public void CallTaxi(int ID)
        {
            this.GoingToId = ID;
            RoomData Room = PlusEnvironment.GetGame().GetRoomManager().GenerateRoomData(ID);
            if (Room == null)
                return;

            this.Chat("*Whistles for a taxi to " + Room.Name + "*");

            new Thread(() =>
            {
                Thread.Sleep(10000);
                this.GetClient.GetHabbo().Credits -= 15;
                this.GetClient.SendMessage(new CreditBalanceComposer(this.GetClient.GetHabbo().Credits));
                this.GetClient.GetHabbo().PrepareRoom(ID, "");
            }).Start();
        }

        public void ArrestUser(int Time, bool JustEntered = false)
        {
            if (this.IsWorking)
                this.StopWorkTimer(this.GetClient);

            this.InJail = true;
            this.JailAmount = Time;
            this.Wanted = false;
            this.SentencedAt = DateTime.Now;

            JailTimer = new System.Timers.Timer();
            JailTimer.AutoReset = false;
            JailTimer.Elapsed += JailTimer_Elapsed;
            JailTimer.Interval = (Time * 60) * 1000;
            

            JailTask = new Task(JailTimer.Start);
            JailTask.Start();

            if (!JustEntered)
                this.GetClient.GetHabbo().PrepareRoom(PlusStaticGameSettings.JailId, "");
            else
                this.LoggedOffInJail = true;
        }

        public void StartWorkTimer(int Pay)
        {
            GetClient.GetHabbo().Roleplay.OnTheClock = DateTime.Now;
            WorkTimer = new System.Timers.Timer();
            WorkTimer.Interval = 60000;
            WorkTimer.AutoReset = false;
            WorkTimer.Elapsed += WorkTimer_Elapsed;

            Payment = Pay;

            Task WorkTask = new Task(WorkTimer.Start);
            WorkTask.Start();
        }


        public void JailTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            JailTimer.Dispose();
            JailTask.Dispose();

            this.InJail = false;
            this.GetClient.GetHabbo().PrepareRoom(342, "");
        }

        public void PayUser()
        {
            if (GetClient.GetHabbo().Roleplay.IsWorking)
            {
                if (GetClient.GetHabbo().TimeAFK > 5)
                {
                    StopWorkTimer(GetClient);
                    GetClient.GetHabbo().Roleplay.Chat("*Stops working due to being afk for " + GetClient.GetHabbo().TimeAFK + "*");
                    GetClient.SendWhisper("You have been afk for 5 minutes and automatically stopped working.");
                    return;
                }

                int Amount = GetClient.GetHabbo().Roleplay.Payment;
                GetClient.GetHabbo().Credits += Amount;
                GetClient.SendMessage(new CreditBalanceComposer(GetClient.GetHabbo().Credits));

                GetClient.GetHabbo().Roleplay.Chat("*Has been paid*");
                StartWorkTimer(Payment);
            }
        }

        public void WorkTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PayUser();
        }

        public void StopWorkTimer(GameClient Session)
        {
            if (WorkTimer != null || WorkTimer.Enabled)
            {
                WorkTimer.Stop();
            }
        }

        public void CreateGang(GameClient Session, string GangName)
        {
            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                Adapter.SetQuery("SELECT * FROM rp_gangs WHERE gang_name = @name");
                Adapter.AddParameter("name", GangName);
                Adapter.RunQuery();

                if (Session.GetHabbo().Roleplay.HasGang)
                {
                    Session.SendWhisper("Please delete your current gang to create a new one!");
                    return;
                }

                DataRow Row = Adapter.getRow();

                if (Row == null)
                {
                    Adapter.SetQuery("INSERT INTO `rp_gangs` (`owner_id`, `gang_name`, `kills`) VALUES ('" + Session.GetHabbo().Id + "',@name,0)");
                    Adapter.AddParameter("name", GangName);
                    Adapter.RunQuery();

                    Session.SendWhisper("You have successfully created the gang " + GangName + "!");
                    Session.GetHabbo().Roleplay.GangName = GangName;
                }
                else
                {
                    Session.SendWhisper("That gang exists already!");
                    return;
                }
            }
        }

        public void DeleteGang(GameClient Session, string CurrentGang)
        {
            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                Adapter.SetQuery("SELECT * FROM rp_gangs WHERE owner_id = @id");
                Adapter.AddParameter("id", Session.GetHabbo().Id);

                DataRow Row = Adapter.getRow();
                if (Row == null)
                    return;

                string GangName = Convert.ToString(Row["gang_name"]);
                Row.Delete();

                Session.SendWhisper("You have deleted your gang " + GangName + "!");
            }
        }

        public void MoreGangChanges(GameClient Session)
        {
            using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
            {
                Adapter.SetQuery("SELECT * FROM rp_gangs WHERE owner_id = @id");
                Adapter.AddParameter("id", Session.GetHabbo().Id);

                DataRow Row = Adapter.getRow();
                if (Row == null)
                    return;

                int GangId = Convert.ToInt32(Row["id"]);
                Session.GetHabbo().Roleplay.MyGangId = GangId;
            }
        }

        public void InviteMember(GameClient TargetClient, GameClient Session)
        {
            if (TargetClient.GetHabbo().Roleplay.GangId > 0 || TargetClient.GetHabbo().Roleplay.HasPendingInvite)
            {
                Session.SendWhisper("This user has a outstanding invite to a gang!");
                return;
            }
            else
            {
                TargetClient.GetHabbo().Roleplay.HasPendingInvite = true;
                TargetClient.GetHabbo().Roleplay.InvitedToId = Session.GetHabbo().Roleplay.MyGangId;

                Session.GetHabbo().Roleplay.Chat("*Invites " + TargetClient.GetHabbo().Username + " to their gang*");
                TargetClient.SendNotification("You have been invited to " + Session.GetHabbo().Username + "'s type :acceptinvite to accept.");
                TargetClient.GetHabbo().Roleplay.InvitedBy = Session.GetHabbo().Username;
                TargetClient.GetHabbo().Roleplay.InvitedToName = Session.GetHabbo().Roleplay.MyGangName;
            }
        }

        public void AcceptInvite(GameClient Session)
        {
            if (Session.GetHabbo().Roleplay.HasPendingInvite && Session.GetHabbo().Roleplay.GangId > 0)
            {
                using (IQueryAdapter Adapter = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                {
                    Adapter.SetQuery("INSERT INTO `group_members` (`userid`, `gang_id`, `gang_rank`) VALUES (@userid, @gangid, @gangrank)");
                    Adapter.AddParameter("userid", Session.GetHabbo().Id);
                    Adapter.AddParameter("gangid", Session.GetHabbo().Roleplay.InvitedToId);
                    Adapter.AddParameter("gangrank", 1);

                    Session.GetHabbo().Roleplay.HasPendingInvite = false;
                    Session.GetHabbo().Roleplay.GangId = Session.GetHabbo().Roleplay.InvitedToId;

                    Session.GetHabbo().Roleplay.HasPendingInvite = false;
                    Session.GetHabbo().Roleplay.InvitedToId = 0;
                    Session.GetHabbo().Roleplay.InvitedBy = String.Empty;

                    Session.GetHabbo().Roleplay.Chat("*Accepts the invite to join the gang " + GangName + "*");
                    Session.GetHabbo().Roleplay.InvitedToName = String.Empty;
                }
            }
        }

   
        public void StartFishingTimer(GameClient Session)
        {
            FishingTimer = new System.Timers.Timer();
            FishingTimer.Interval = 600000;
            FishingTimer.AutoReset = false;
            FishingTimer.Elapsed += FishingTimer_Elapsed;

            FishingTask = new Task(FishingTimer.Start);
            FishingTask.Start();

            Session.GetHabbo().Roleplay.StartedFishing = DateTime.Now;
        }

        public void CompleteFishing(GameClient Session)
        {
            if (Session.GetHabbo().Roleplay.FishAmount >= 50)
            {
                Session.SendWhisper("You cannot collect anymore fish! (max 50)");
                return;
            }
            else
            {
                Random FishRandomizer = new Random();
                int Fish = FishRandomizer.Next(1, 7);

                Session.GetHabbo().Roleplay.FishAmount += Fish;
            }
        }

        public void FishingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            GameClient Session = PlusEnvironment.GetGame().GetClientManager().GetClientByUserID(this.UserId);

            CompleteFishing(Session);
        }
    }
}